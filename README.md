# wekan-backup

[![Build Status](https://ci.wendelin.dev/api/badges/masterwendu/wekan-backup/status.svg)](https://ci.wendelin.dev/masterwendu/wekan-backup)

Download all wekan boards from a single user as json export and backup it to nextcloud (or another webdav service)

This service runs in combination with a drone CI, in drone CI you have to create a scheduled job to run it for example every day.

## SETUP ENVIRONMENT

- use node version 14.

### ENVIRONMENT VARIABLES

- `WEKAN_USERNAME`, example (`WEKAN_USERNAME=wendelin`)
- `WEKAN_PASSWORD`, example (`WEKAN_PASSWORD=supersecret`)
- `NEXTCLOUD_USERNAME`, example (`NEXTCLOUD_USERNAME=wendelin`)
- `NEXTCLOUD_PASSWORD`, example (`NEXTCLOUD_PASSWORD=supernextcloudsecret`)
- `BACKUP_FOLDER_PATH`, example (`BACKUP_FOLDER_PATH=/Backups/wekan/`) `/` at the end is important
- `NEXTCLOUD_WEBDAV_URL`, example (`NEXTCLOUD_WEBDAV_URL=https://mynextcloud.com/remote.php/dav/files/wendelin/`) `/` at the end is important
- `WEKAN_URL`, example (`WEKAN_URL=https://mywekan.com/`) `/` at the end is important