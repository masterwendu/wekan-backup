const { create } = require('apisauce')
const fs = require('fs')
const { promises: fsAsync } = require('fs')
const sanitize = require("sanitize-filename")
const Downloader = require('nodejs-file-downloader')
const rimraf = require("rimraf")
const dayjs = require("dayjs")
const { zip } = require("zip-a-folder")
const { createClient } = require("webdav")

// define the api to connect to wekan
const api = create({
  baseURL: process.env.WEKAN_URL,
  headers: { Accept: 'application/vnd.github.v3+json', 'content-type': 'application/json' },
})

const backup = async () => {
  try {
    // login to wekan to get a bearer token
    const { data: { id, token } } = await api.post('/users/login', 
    {
        "username": process.env.WEKAN_USERNAME,
        "password": process.env.WEKAN_PASSWORD,
    })
  
    // set the token for all request headers
    const headers = { Authorization: `Bearer ${token}` }
  
    // get all wekan boards of the user
    const { data: boards } = await api.get(`/api/users/${id}/boards`, {}, { headers })
  
    // prepare folder to store exports
    rimraf.sync('./exports')
  
    // for each board export to json and save it in the exports folder
    await Promise.all(boards.map(async (board) => {
      const downloader = new Downloader({
        url: `${process.env.WEKAN_URL}api/boards/${board._id}/export?authToken=${token}`,
        directory: "./exports/files",
        fileName: `${sanitize(board.title)}.json`, 
      })
      try {
        await downloader.download()
      } catch (error) {
        console.log('Download failed', error)
      }
    }))
  
    // archive exports to a zip folder and name it with the current date
    const date = dayjs()
    const filename = `${date.year()}-${date.month() + 1}-${date.date()}_wekanExports_${process.env.WEKAN_USERNAME}.zip`
    await zip('./exports/files', `./exports/${filename}`)
  
    // connect to nextcloud(webdav)
    const webDavClient = createClient(
      process.env.NEXTCLOUD_WEBDAV_URL,
      {
          username: process.env.NEXTCLOUD_USERNAME,
          password: process.env.NEXTCLOUD_PASSWORD,
      }
    )
  
    // upload the zip file to nextcloud
    const zipFile = await fsAsync.readFile(`./exports/${filename}`)
    await webDavClient.putFileContents(`${process.env.BACKUP_FOLDER_PATH}${filename}`, zipFile, { overwrite: true })

    // define date for last backup
    const cutoffDate = dayjs().subtract(60, 'days')

    // delete all older backups then the defined day
    const allBackups = await webDavClient.getDirectoryContents(process.env.BACKUP_FOLDER_PATH)
    await Promise.all(allBackups.map(async ( { lastmod, filename }) => {
      if (cutoffDate.isAfter(dayjs(lastmod))) {
        return webDavClient.deleteFile(filename)
      }
      return new Promise((resolve) => { resolve() })
    }))
  } catch (error) {
    console.log(error)
    throw error
  }
}

backup()